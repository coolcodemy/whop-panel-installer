$(document).ready(function()
{

	function populate()
	{
		var dn = $("#domainName").val();
		var date = new Date();
		var serverIP = $("#serverIpAddress").val();
		var serverName = $("#serverName").val();

		var month = date.getMonth() + 1;

		var html = "\
		<table class='table'>\
			<thead>\
				<tr>\
					<th>Record Name</th>\
					<th>Type</th>\
					<th>Content</th>\
					<th>TTL</th>\
					<th>Priority</th>\
				</tr>\
			</thead>\
			<tbody>\
				<tr>\
					<td>" + dn + "</td>\
					<td>SOA</td>\
					<td>ns1." + dn + " root.ns1." + dn + " " + date.getFullYear() + month + date.getDate() + "01 86400 7200 604800 300</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + dn + "</td>\
					<td>NS</td>\
					<td>ns1." + dn + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + dn + "</td>\
					<td>NS</td>\
					<td>ns2." + dn + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>ns1." + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>ns2." + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>www." + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + serverName + "." + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + dn + "</td>\
					<td>MX</td>\
					<td>" + dn + "</td>\
					<td>600</td>\
					<td>10</td>\
				</tr>\
				<tr>\
					<td>*." + dn + "</td>\
					<td>A</td>\
					<td>" + serverIP + "</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
				<tr>\
					<td>" + dn + "</td>\
					<td>TXT</td>\
					<td>v=spf1 mx a -all</td>\
					<td>86400</td>\
					<td></td>\
				</tr>\
			</tbody>\
		</table>\
		";
		$("#wellDNS").html(html);
	}

	$("#domainName").keyup(function()
	{
		setServerName();
		populate();
	});


	$("#serverName").keyup(function()
	{
		populate();
	});


	function setServerName()
	{
		$("#domain").html('.' + $('#domainName').val());
	}


	$("body").on('click', '#startButton', function()
	{
		$("#startButton").addClass('hide');

		$("#messages").append("<p>Please wait until the installation finish!</p>");


		$.ajax({
		    cache: false,
		    url: "/config.json",
		    dataType: "json",
		    success: function(json) {
				socket.emit('buildServer-server', json);
		    }
		});

	});
});