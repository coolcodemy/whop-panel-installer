@extends('layouts.master')


@section('content')

	<div class="alert alert-warning" role="alert">Please fill everything correctly. It won't be check either right or wrong value</div>

	<div class="alert alert-success" role="alert">
		This is admin account for WHoP<sup>&reg;</sup>. This is the only user which have much power
		to do everything in the panel. And this is the <strong>only</strong> user that have Super Admin privilege. You can add other admin yourself by modifying SQL table, but we don't encourage that.
		Besides that, the other user is Reseller and Domain Owner.
	</div>



	{!! Form::open( ['route' => 'step1-commit', 'class' => 'form-horizontal', 'role' => 'form']) !!}
		
		@if ($errors->has())
		<div class="alert alert-danger" role="alert">
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>	
			@endforeach	
		</div>
		@endif
		<div class="page-header">
			<h4>WHoP<sup>&reg;</sup> Panel Admin</h4>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Super Admin Username</label>
			<div class="col-sm-10">
				<input type="text" name="username" id="username" placeholder="Eg: admin" class="form-control" value="{{ old('username') }}" required>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Super Admin Password</label>
			<div class="col-sm-10">
				<input type="password" name="password" id="password" class="form-control" required>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Super Admin Password (Verification)</label>
			<div class="col-sm-10">
				<input type="password" name="passwordVerification" id="passwordVerification" class="form-control" required>
			</div>
		</div>


		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default pull-right">Submit</button>
			</div>
		</div>
	{!! Form::close() !!}


@stop