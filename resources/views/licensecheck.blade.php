@extends('layouts.master')


@section('content')

	<div class="alert alert-success" role="alert">
		You can download your free license from <a href="https://whop.io/">WHoP.io Website</a>. You need to register as customer to create free license. No credit card required at all.
	</div>

	@if ( Session::has('successMessage') )
		<div class="alert alert-info" role="alert">
			{{ Session::get('successMessage') }}
		</div>
	@endif

	<div class="alert alert-success hide" id="success" role="alert">
		Succesfully verify your license. Redirecting...
	</div>

	<div class="alert alert-danger hide" id="fail" role="alert">
		Fail to verify your license. Please upload WHoP License...
	</div>

	{!! Form::open( ['route' => 'licenseupload', 'role' => 'form', 'files' => true]) !!}
		<div class="page-header">
			<h4>License Uploader</h4>
		</div>


		<div class="form-group">
			<label for="exampleInputFile">Choose WHoP license</label>
			<input type="file" id="exampleInputFile" name="licenseFile">
			<span class="text-danger">{{ $errors->first('licenseFile') }}</span>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" id="submitbutton" class="btn btn-default pull-right">Submit</button>
			</div>
		</div>


	{!! Form::close() !!}
@stop



@section('script')
	<script src="/socket.io/socket.io.js"></script>

	<script>
		var socket = io();

		socket.emit('verify-license-server', []);

		socket.on('verify-license-client', function(res) {

			console.log(res);

			if ( res == true ) {

				$("#submitbutton").addClass('hide');

				$("#success").removeClass('hide');

				setTimeout(function() {
					window.location.replace('/1');
				}, 2000);

			} else {

				$("#fail").removeClass('hide');

			}
		});
	</script>
@stop