@extends('layouts.master')


@section('content')

	<div class="page-header">
		<h4>End-User License Agreement for WHoP<sup>&reg;</sup> (Web Hosting Panel)</h4>
	</div>
	<p>By installing and/or using <strong>WHoP<sup>&reg;</sup></strong> (this software) you are agree to End-User License Agreement (EULA) stated below:</p>
	<div class="well" style="height:300px;overflow:auto">
		<p>
			This End-User License Agreement (EULA) is a legal agreement between you (either an individual or a single entity) and the mentioned author (Cool Code Sdn.Bhd.) of this Software for the software product identified above, which includes computer software and may include associated media, printed materials, and "online" or electronic documentation.<br><br>

			By installing, copying, or otherwise using the WHoP<sup>&reg;</sup>, you agree to be bounded by the terms of this EULA. If you do not agree to the terms of this EULA, do not install or use the WHoP<sup>&reg;</sup>.
		</p>
		<p>
			WHoP<sup>&reg;</sup> LICENSE
		</p>
		<p>
			a) WHoP<sup>&reg;</sup> is being distributed as freen and paid software for personal, commercial use, non-profit organization and educational purpose. You are <strong>NOT</strong> allowed to make a charge for distributing this Software (either for profit or merely to recover your media and distribution costs) whether as a stand-alone product, or as part of a compilation or anthology, nor to use it for supporting your business or customers. It may be distributed by any website but the download link must point to WHoP<sup>&reg;</sup> Offical Website.
		</p>
		<p>
			b) This software does not include any license key. To use it, it is nessesary to download and generate your own license key in <a href="https://whop.io" target="_blank">WHoP.io Website</a>. There is limitation on this license and the upgrade is available by purchasing other type of license from WHoP<sup>&reg;</sup> Official Website.
		</p>
		<p>
			1. <strong>GRANT OF LICENSE</strong>. This software product includes two types of computer software (1) software that is owned by Cool Code Sdn. Bhd. (and may include associated media, and "online" or electronic documentation) (collectively the "WHoP<sup>&reg;</sup>") and (2) other software provided by third parties and used with the WHoP<sup>&reg;</sup> ("Third Party Software"). Cool Code Sdn. Bhd. grants you the following non-exclusive rights provided you agree to and comply with all terms and conditions of this EULA:
		</p>
		<ul class="list-unstyled" style="margin-left:15px">
			<li>
				a) <strong>Use</strong>. You may use the WHoP<sup>&reg;</sup> on your computer/virtual server/server (or computer/virtual server/server if the WHoP<sup>&reg;</sup> is sold to you). You may use the WHoP<sup>&reg;</sup> on additional computer/virtual server/server. You have right to tell people about WHoP<sup>&reg;</sup> for them to download but you do not have the right to distribute the WHoP<sup>&reg;</sup>. You agree to only use the WHoP<sup>&reg;</sup> as expressly permitted herein. 
			</li>
			<li>
				b) <strong>Reservation of rights</strong>. The WHoP<sup>&reg;</sup> is licensed, not sold, to you by Cool Code Sdn. Bhd.. Cool Code Sdn. Bhd. and its suppliers own all right, title and interest in and to the WHoP<sup>&reg;</sup> and reserve all rights not expressly granted to you in this EULA. You agree to refrain from any action that would diminish such rights or would call them into question.
			</li>
			<li>
				c) <strong>Third party software</strong>. Notwithstanding the terms and conditions of this EULA, all or any portion of the WHoP<sup>&reg;</sup> which constitutes Third Party Software, is licensed to you subject to the terms and conditions of the software license agreement accompanying such Third Party Software whether in the form of a discrete agreement, shrink wrap license or electronic license terms accepted at time of download. Use of the Third Party Software by you shall be governed entirely by the terms and conditions of such license.
			</li>
			<li>
				d) <strong>Technical Support</strong>. Technical support will be given by WHoP to you from WHoP Website. Feel free to ask us anything from our website.
			</li>
		</ul>
		<p>
			2. <strong>UPGRADES</strong>. Software upgrades will be provided to you but not automatically unless you are using license key that support automatic upgrade.
		</p>
		<p>
			3. <strong>ADDITIONAL SOFTWARE</strong>. This EULA applies to updates or supplements to the original WHoP<sup>&reg;</sup> provided by Cool Code Sdn. Bhd. unless Cool Code Sdn. Bhd. provides other terms along with the update or supplement. In case of a conflict between such terms, the other terms will prevail.
		</p>
		<p>
			4. <strong>TRANSFER</strong>. 
		</p>
		<ul class="list-unstyled" style="margin-left:15px">
			<li>
				a) <strong>Third Party</strong>. The WHoP<sup>&reg;</sup> may only be transferred to another end user as part of a transfer of the computer(s) on which it is installed. Any transfer must include all component parts, media, printed materials and this EULA. Prior to the transfer, the end user receiving the transferred product must agree to all the EULA terms. Upon transfer of your computer(s), your license is automatically terminated and you are no longer permitted to use the WHoP<sup>&reg;</sup>.
			</li>
			<li>
				b) <strong>Restrictions</strong>. You may not rent, lease or lend the WHoP<sup>&reg;</sup> or use the WHoP<sup>&reg;</sup> for commercial timesharing or bureau use. You may not sublicense, assign or transfer the license or WHoP<sup>&reg;</sup> except as expressly provided in this EULA.
			</li>
		</ul>
		<p>
			5. <strong>PROPRIETARY RIGHTS</strong>. All intellectual property rights in the WHoP<sup>&reg;</sup> and user documentation are owned by Cool Code Sdn. Bhd. or its suppliers and are protected by law, including but not limited to copyright, trade secret, and trademark law, as well as other applicable laws and international treaty provisions. The structure, organization and code of the WHoP<sup>&reg;</sup> are the valuable trade secrets and confidential information of Cool Code Sdn. Bhd. and its suppliers. You shall not remove any product identification, copyright notices or proprietary restrictions from the WHoP<sup>&reg;</sup>. 
		</p>
		<p>
			6. <strong>LIMITATION ON REVERSE ENGINEERING</strong>. Except to the extent that such restriction is not permitted under applicable law, you are not permitted (and you agree not to) reverse engineer, decompile, disassemble or create derivative works of or modify the WHoP<sup>&reg;</sup>. Nothing contained herein shall be construed, expressly or implicitly, as transferring any right, license or title to you other than those explicitly granted under this EULA. Cool Code Sdn. Bhd. reserves all rights in its intellectual property rights not expressly agreed to herein. Unauthorized copying of the WHoP<sup>&reg;</sup> or failure to comply with the restrictions in this EULA (or other breach of the license herein) will result in automatic termination of this Agreement and you agree that it will constitute immediate, irreparable harm to Cool Code Sdn. Bhd. for which monetary damages would be an inadequate remedy, and that injunctive relief will be an appropriate remedy for such breach.
		</p>
		<p>
			7. <strong>TERM</strong>. This EULA is effective unless terminated or rejected. This EULA will also terminate immediately and without additional notice in the event you breach this EULA and/or fail to comply with any term or condition of this EULA.
		</p>
		<p>
			8. <strong>CONSENT TO USE OF DATA</strong>. You agree that Cool Code Sdn. Bhd. and its affiliates or suppliers may collect and use statistics on your use of the WHoP<sup>&reg;</sup> in performing backup operations and technical information you provide in relation to support services related to the WHoP<sup>&reg;</sup>.
		</p>
		<p>
			9. <strong>DISCLAIMER OF WARRANTIES</strong>.
		</p>
		<ul class="list-unstyled" style="margin-left:15px">
			<li>
				a) WHoP<sup>&reg;</sup> comes with no warranty that it will not harm your computer/virtual server/server Operating System, software or hardware. Any fault and/or damage happen when using or after using WHoP<sup>&reg;</sup> is not responsibility of Cool Code Sdn. Bhd. or any entity that related to them. 
			</li>
			<li>
				b) Cool Code Sdn. Bhd. did not responsible to any data loss (either you or your client) while or after using this software either after you are uninstalling or still using it. WHoP<sup>&reg;</sup> distributed "as is" and do not have any warranty for loss data and/or hardware fault. If you were hacked while or after using WHoP<sup>&reg;</sup>, Cool Code Sdn. Bhd. did not have warranty for it.
			</li>
			<li>
				c) IN NO EVENT DOES Cool Code Sdn. Bhd. PROVIDE ANY WARRANTY OR REPRESENTATIONS WITH RESPECT TO ANY THIRD PARTY HARDWARE OR SOFTWARE WITH WHICH THE WHoP<sup>&reg;</sup> IS DESIGNED TO BE USED, AND Cool Code Sdn. Bhd. DISCLAIMS ALL LIABILITY WITH RESPECT TO ANY FAILURES THEREOF. 
			</li>
		</ul>
		<p>
			10. <strong>INDEMNITY</strong>. You agree to indemnify and hold Cool Code Sdn. Bhd. and its officers, directors, employees and licensors harmless from any claim or demand (including but not limited to reasonable legal fees) made by a third party due to or arising out of or related to your violation of the terms and conditions of this Agreement, your violation of any laws, regulations or third party rights or your negligent act, omission or willful misconduct. 
		</p>
		<p>
			11. <strong>COMPLIANCE WITH LAWS</strong>. You shall comply with all laws and regulations of the Malaysia and other countries ("Export Laws") to ensure that the WHoP<sup>&reg;</sup> is not (1) exported, directly or indirectly, in violation of Export Laws, or (2) used for any purpose prohibited by Export Laws, including, without limitation, nuclear, chemical, or biological weapons proliferation. You further agree that you will not use the WHoP<sup>&reg;</sup> for any purpose prohibited under applicable law.
		</p>
		<p>
			12. <strong>CHANGING OF EULA TERM</strong>. Cool Code Sdn. Bhd. by any mean can change this EULA condition for our own benefit. You also bound to agree to this newly written EULA when your are using this software.
		</p>
	</div>

	<div class="page-header">
		<h4>Introduction</h4>
	</div>

	<p>
		1. What is <strong>WHoP<sup>&reg;</sup></strong>? You can say that <strong>WHoP<sup>&reg;</sup></strong> is an alternative to WHM/cPanel, Plesk Panel and other web hosting panel. It is a web hosting panel and that is it's kind of software. <strong>WHoP<sup>&reg;</sup></strong> derived it's name from Web Hosting Panel .
	</p>
	<p>
		2. There are so many free and open source web hosting panel, why we built another one? Most panel have some problem that need to be fixed before installing the panel. Sometimes the panel
		does not work at all. This will be a frustration after spending much time on fixing and Googling. I know, I have been there before. But one day I thought why I did not build my own panel 
		with optimized setting that I want? Then there it is, <strong>WHoP<sup>&reg;</sup></strong>.
	</p>
	<p>
		3. Why reinvent the wheel? It is not about reinventing back the wheel. Most panel use apache/httpd as the web server. But if they use nginx, it is not natively using nginx but it creates a proxy for other web server (E.g: Apache), and <strong>WHoP<sup>&reg;</sup></strong> itself is using Nginx as the main web server in shared environment. Nginx itself does not build for 
		shared hosting, but we have made the research and does test ourself about the website performance speed over server performance. Nginx does better and speed up everything 
		while leaving apache/httpd behind. And hey, why not using it in shared environment? To solve this problem, <strong>WHoP<sup>&reg;</sup></strong> have made Nginx template 
		for server admin. User just need to select the right Nginx template to use with their website.
	</p>
	<p>
		4. Is this panel a noob proof? Basically security setting have been tweak while you installing this panel before. You do not have to worry about it again. For Nginx configuration, some of server admin
		have problem with it. You will not generating wrong Nginx config with WHoPlet&trade; Template Manager.
	</p>

	<div class="page-header">
		<h4>What do I get from <strong>WHoP<sup>&reg;</sup></strong></h4>
	</div>

	<p><strong>Service</strong></p>
	<ol>
		<li><strong>WHoP<sup>&reg;</sup></strong> Panel</li>
		<li>MariaDB Server with tweaked setting and phpMyAdmin as configuration interface</li>
		<li>Power DNS as DNS Server</li>
		<li>Nginx as Web Server with tweaked setting</li>
		<li>Postfix &amp; Dovecot</li>
		<li>Pure-FTPD</li>
		<li>Roundcube Web Email Client</li>
		<li>Secure Shell</li>
	</ol>

	<p><strong>Security</strong></p>
	<ol>
		<li>Auto secure <strong>WHoP<sup>&reg;</sup></strong> Panel</li>
		<li>Suhosin Patch</li>
		<li>Postfix & Dovecot on secure port only</li>
		<li>Spamassassin (auto configure and auto update)</li>
		<li>CSF Firewall (auto configure)</li>
		<li>Fail2Ban (auto configure)</li>
		<li>Jail user in their own environment</li>
	</ol>

	<p><strong>Others</strong></p>
	<ol>
		<li>Disk quota management</li>
		<li>FTP Quota</li>

	</ol>

	<div class="page-header">
		<h4>Requirement</h4>
	</div>

	<p>
		Before you proceed, please make sure you have add your domain name server to ns1.yourdomainname and ns2.yourdomainname to {{ Request::server('SERVER_ADDR') }} as glue record. Please contact your domain name provider if you don't know how to setting this glue record.
	</p>
	<p>
		eg: <br>
		ns1.whop.io ---> {{ Request::server('SERVER_ADDR') }} <br>
		ns2.whop.io ---> {{ Request::server('SERVER_ADDR') }}
	</p>

	<div class="page-header">
		<h4>Start</h4>
	</div>

	<a href="{{  route('licensecheck') }}" class="btn btn-success pull-right">Configure</a>
@stop