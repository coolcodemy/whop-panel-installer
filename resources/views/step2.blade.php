@extends('layouts.master')


@section('content')
	<div class="alert alert-warning" role="alert">Please fill everything correctly. It won't be check either right or wrong value</div>
	<div class="alert alert-success" role="alert">
		This is your first user. The domain for this user will be used by WHoP<sup>&reg;</sup> to access your panel. But this user will have no power to 
		modify your WHoP<sup>&reg;</sup> panel. You must treat this user as yourself.
	</div>

	<div class="alert alert-danger" role="alert">
		Please do not input username that is used by OS (e.g: nginx, root, redis) or whop.
	</div>

	{!! Form::open( ['route' => 'step2-commit', 'class' => 'form-horizontal', 'role' => 'form']) !!}
		<div class="page-header">
			<h4>Account</h4>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10">
				<input type="text" name="name" value="{{ old('name') }}" placeholder="Eg: John Doe" class="form-control" required>
				<span class="text-danger">{{ $errors->first('name') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Username</label>
			<div class="col-sm-10">
				<input type="text" name="username" placeholder="Eg: johndoe" class="form-control" value="{{ old('username') }}" required>
				<span class="text-danger">{{ $errors->first('username') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input type="password" name="password"  class="form-control" required>
				<span class="text-danger">{{ $errors->first('password') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Password (Verification)</label>
			<div class="col-sm-10">
				<input type="password" name="passwordVerification"  class="form-control" required>
				<span class="text-danger">{{ $errors->first('passwordVerification') }}</span>
			</div>
		</div>


		<div class="page-header">
			<h4>Domain Name</h4>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Domain name</label>
			<div class="col-sm-10">
				<input type="text" name="domainName" value="{{ old('domainName') }}" id="domainName" placeholder="Eg: whop.my" class="form-control" required>
				<span class="text-danger">{{ $errors->first('domainName') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Server Name</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" name="serverName" value="{{ old('serverName') }}" id="serverName" placeholder="Eg: server" class="form-control" required>
					<span class="input-group-addon"><span id="domain"></span></span>
				</div>
				<span class="text-danger">{{ $errors->first('serverName') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">Primary Email</label>
			<div class="col-sm-10">
				<input type="email" name="primaryEmail" id="primaryEmail" value="{{ old('primaryEmail') }}" placeholder="Eg: someone@gmail.com" class="form-control" required>
				<span class="text-danger">{{ $errors->first('primaryEmail') }}</span>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">DNS Record</label>
			<div class="col-sm-10">
				<div class="well" id="wellDNS">...</div>
			</div>
		</div>

		<input type="hidden" id="serverIpAddress" name="serverIpAddress" value="{{ Request::server('SERVER_ADDR') }}">

		

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default pull-right">Submit</button>
			</div>
		</div>
	{!! Form::close() !!}
@stop