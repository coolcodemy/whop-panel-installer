@extends('layouts.master')





@section('content')
	<div class="alert alert-success" role="alert">After installing this software, you won't be able to go back to previous page. Please make sure you have input the right value. Press start button when you are ready!</div>
	<hr>

	<div id="messages">
	</div>

	<button id="startButton" class="btn btn-success pull-right">Start!</button>
	<a href="https://{{ $serverName }}:3025/" id="goto" class="btn btn-success pull-right hide">View WHoP Panel</a>
@stop










@section('script')
	<script src="/socket.io/socket.io.js"></script>

	<script>
		var socket = io();

		socket.on('buildServer-client', function(msg)
		{
			if(msg == 'FINISH') {
				$("#goto").removeClass("hide");

				$.ajax({
				    cache: false,
				    url: "/config.json",
				    dataType: "json",
				    success: function(json) {
						$("#goto").removeClass("hide");
						$("#goto").attr("href", "https://" + json.ServerName + ":3025");
				    }
				});

			} else {
				$("#messages").append("<p>" + msg + "</p>");
			}
		});	
	</script>
@stop