<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'InstallController@start']);


Route::get('licensecheck', ['as' => 'licensecheck', 'uses' => 'InstallController@licenseCheck']);
Route::post('licenseupload', ['as' => 'licenseupload', 'uses' => 'InstallController@licenseStore']);



Route::get('/1', ['as' => 'step1', 'uses' => 'InstallController@step1']);
Route::post('/1/commit', ['as' => 'step1-commit', 'uses' => 'InstallController@step1Commit']);
Route::get('/2', ['as' => 'step2', 'uses' => 'InstallController@step2']);
Route::post('/2', ['as' => 'step2-commit', 'uses' => 'InstallController@step2Commit']);
Route::get('/3', ['as' => 'step3', 'uses' => 'InstallController@step3']);