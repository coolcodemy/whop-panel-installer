<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;


use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

class InstallController extends Controller
{
	private $blacklist = "root,bin,daemon,adm,lp,sync,shutdown,halt,mail,uucp,operator,games,gopher,ftp,nobody,dbus,usbmuxd,vcsa,rpc,rtkit,avahi,abrt,prcuser,nsfnobody,haldaemon,gdm,ntp,apache,saslauth,postfix,pulse,sshd,tcpdump,whop,nginx,whopemail,mysql,pdns,memcached,dovecot,dovenull,spamd,varnish,clam,spamd,amavis";


	private function writer($array)
	{
		$json = json_decode(file_get_contents('config.json'));

		foreach ($array as $key => $value) {
			$json->$key = $value;

		}

		$json = json_encode($json, JSON_PRETTY_PRINT);
		$configFile = fopen("config.json", "w") or die("Unable to open file!");
		fwrite($configFile, $json);
		fclose($configFile);
	}

    public function start()
    {
    	copy("config.json-sample","config.json");
    	return view('index', ['title' => 'Welcome to WHoP Installation']);
    }


    public function licenseCheck()
    {
        return view('licensecheck', ['title' => 'Please upload your license']);
    }


    public function licenseStore(Request $request)
    {
        $this->validate($request, [
            'licenseFile' => 'required'
        ]);

        $request->file('licenseFile')->move(public_path(), 'license.tar.gz');

        $client = new Client(new Version1X('http://127.0.0.1:30000'));
        $client->initialize();
        $client->emit('upload-server', ['licensePath' => '/home/whop/panel/whop-panel-installer/public/license.tar.gz']);
        $client->close();

        return redirect()->back()->with([
            'successMessage' => 'Please wait while validating your license...'
        ]);
    }

    public function step1()
    {
        return view('step1', ['title' => 'Admin Account']);
    }

    public function step1Commit(Request $request)
    {
        $rules = [
            'username' => 'required|alpha_num|min:5|max:32',
            'password' => 'required|min:5',
            'passwordVerification' => 'required|same:password',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $arr = array(
            'AdminUsername' => $request->input('username'),
            'AdminPassword' => $request->input('password')
            );

        $this->writer($arr);

        return redirect()->route('step2');
    }


    public function step2()
    {
    	return view('step2', ['title' => 'User Account']);
    }

    public function step2Commit(Request $request)
    {
    	$rules = [
    		'name' => 'required|min:5|max:64',
    		'username' => 'required|alpha_num|min:3|max:8|not_in:' . $this->blacklist,
    		'password' => ['required', 'min:5'],
    		'passwordVerification' => 'required|same:password',
    		'domainName' => 'required|regex:/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/',
            'serverName' => 'required|alpha_num',
    		'primaryEmail' => 'required|email',
    		'serverIpAddress' => 'required|ip',
    	];

    	$validator = Validator::make($request->all(), $rules);

    	if ($validator->fails()) {
    		return redirect()->back()->withInput()->withErrors($validator);
    	}

    	$request->merge([
    		'username' => strtolower($request->input('username')),
    		]);


		$arr = array(
			'DomainName' => $request->input('domainName'),
            'ServerName' => $request->input('serverName') . '.' . $request->input('domainName'),
			'PrimaryEmail' => $request->input('primaryEmail'),
			'ServerIP' => $request->input('serverIpAddress'),
			'AccountName' => $request->input('name'),
			'AccountUsername' => $request->input('username'),
			'AccountPassword' => $request->input('password'),
			);

		$this->writer($arr);

		return redirect()->route('step3');
    }


    public function step3()
    {
    	$json = json_decode(file_get_contents('config.json'));

    	return view('step3', ['title' => 'Finalize Installation', 'serverName' => $json->ServerName]);
    }
}
